
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"

#define TEST_SIZE_HEAP  600
#define MEM_TEST_SIZE_100 100
#define MEM_TEST_SIZE_150 150
#define ADITION_MEM 1000

static inline struct block_header* get_header(void* content)
{
    return (struct block_header*)((uint8_t*)content - offsetof(struct block_header, contents));
};

static bool test1()
{
    fprintf(stdout, "test 1: susscess malloc memory\n");
    fprintf(stdout, "------------------------------------------------------------\n");
    void* heap = heap_init(TEST_SIZE_HEAP);
    void* mem = _malloc(MEM_TEST_SIZE_100);

    if (heap != NULL && mem != NULL) {
        debug_heap(stdout, heap);
        _free(mem);
        if (munmap(heap, size_from_capacity(get_header(mem)->capacity).bytes) != 0) {
            fprintf(stderr, "anable to free heap");
        }
        print_result(SUSSCESS, "test 1");
        return 1;
    }
    fprintf(stderr, "anable to malloc heap or block)");
    print_result(FAILD, "test 0");
    return 0;
}
static bool test2()
{
    fprintf(stdout, "test 2: free a block from some blocks\n");
    fprintf(stdout, "--------------------------------------------------------------\n");
    void* heap = heap_init(TEST_SIZE_HEAP);
    void* mem1 = _malloc(MEM_TEST_SIZE_100);
    void* mem2 = _malloc(MEM_TEST_SIZE_150);

    if (heap == NULL || mem1 == NULL || mem2 == NULL) {
        print_result(FAILD, "test 2");
        return 0;
    }
    fprintf(stdout, "blocks successfully malloced\n");
    debug_heap(stdout, heap);
    fprintf(stdout, "try to free a block\n");
    _free(mem1);
    debug_heap(stdout, heap);
    if (get_header(mem1)->is_free) {
        fprintf(stdout, "block is freed\n");
        _free(mem2);
        if (munmap(heap, size_from_capacity(get_header(mem1)->capacity).bytes + size_from_capacity(get_header(mem2)->capacity).bytes) != 0) {
            fprintf(stderr, "anable to free heap");
        }
        print_result(SUSSCESS, "test 2");
        return 1;
    }
    print_result(FAILD, "test 2");
    return 0;
    
}
static bool test3()
{
    fprintf(stdout, "test 3: free two blocks from some blocks\n");
    fprintf(stdout, "--------------------------------------------------------------\n");
    void* heap = heap_init(TEST_SIZE_HEAP);
    void* mem1 = _malloc(MEM_TEST_SIZE_100);
    void* mem2 = _malloc(MEM_TEST_SIZE_150);

    if (heap == NULL || mem1 == NULL || mem2 == NULL) {
        print_result(FAILD, "test 2");
        return 0;
    }
    fprintf(stdout, "blocks successfully malloced\n");
    debug_heap(stdout, heap);
    fprintf(stdout, "try to free a first block\n");
    _free(mem1);
    debug_heap(stdout, heap);

    if (!get_header(mem1)->is_free) {
        print_result(FAILD, "test 3");
        return 0;
    }
    fprintf(stdout, "block is freed\n");
    fprintf(stdout, "try to free a second block\n");
    debug_heap(stdout, heap);
    _free(mem2);
    if (get_header(mem2)->is_free) {
        fprintf(stdout, "block is freed\n");
        if (munmap(heap, size_from_capacity(get_header(mem1)->capacity).bytes + size_from_capacity(get_header(mem2)->capacity).bytes) != 0) {
            fprintf(stderr, "anable to free heap");
        }
        print_result(SUSSCESS, "test 3");
        return 1;
    }
    print_result(FAILD, "test 3");
    return 0;
    
}

static bool test4()
{
    fprintf(stdout, "test 4: Memory out, new memory region expands old\n");
    fprintf(stdout, "--------------------------------------------------------------\n");
    void* heap = heap_init(TEST_SIZE_HEAP);
    debug_heap(stdout, heap);
    size_t capacity = ((struct block_header*)heap)->capacity.bytes;
    void* mem1 = _malloc(capacity + ADITION_MEM);
    debug_heap(stdout, heap);
    if (capacity < ((struct block_header*)heap)->capacity.bytes) {
        print_result(SUSSCESS, "test 4");
        if (munmap(heap, size_from_capacity(get_header(mem1)->capacity).bytes) != 0) {
            fprintf(stderr, "anable to free heap");
        }
        return 1;
    }
    if (munmap(heap, size_from_capacity(get_header(mem1)->capacity).bytes) != 0) {
        fprintf(stderr, "anable to free heap");
    }
    print_result(FAILD, "test 4");
    return 0;
}
static bool test5()
{
    fprintf(stdout, "test 5: The memory has run out, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated elsewhere\n");
    fprintf(stdout, "--------------------------------------------------------------\n");
    void* heap = heap_init(TEST_SIZE_HEAP);
    debug_heap(stdout, heap);
    void* extra_heap = heap_init_with_offset(TEST_SIZE_HEAP, TEST_SIZE_HEAP);
    debug_heap(stdout, extra_heap);
    
    if (heap == NULL || extra_heap == NULL ) {
        print_result(FAILD, "test 5");
        return 0;
    }

    size_t capacity = ((struct block_header*)heap)->capacity.bytes;
    void* mem1 = _malloc(capacity);
    debug_heap(stdout, heap);
    void* mem2 = _malloc(capacity);
    debug_heap(stdout, heap);
    if (!get_header(mem2)->is_free) {
        print_result(SUSSCESS, "test 5");
        return 1;
    }
     _free(mem1);
     _free(mem2);
     if (munmap(heap, size_from_capacity(get_header(mem1)->capacity).bytes + size_from_capacity(get_header(mem2)->capacity).bytes) != 0) {
        fprintf(stderr, "anable to free heap");
    }

    print_result(FAILD, "test 5");
    return 0;
}

int main()
{
    if (test1() && test2() && test3() && test4() && test5())
        print_result(SUSSCESS, "all tests");
}